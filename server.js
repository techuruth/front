var express = require('express');
var app = express();
var port = process.env.PORT || 2990;

var path = require('path');
//línea para que use los componentes de /build/default
app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log('TechU Front started on port: ' + port);

app.get('/', function(req, res) {
  res.sendFile("index.html", {root:'.'});
});
